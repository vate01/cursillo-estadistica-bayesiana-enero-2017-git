En este repositorio se aloja la presentación del cursillo de estadística bayesiana «Modelos espaciales usando R-INLA» 
ofrecido por el Centro de Ecología Aplicada y Sustentabilidad (CAPES-UC) y el Laboratorio de Tecnología Pesquera 
(TECPES-PUCV) para estudiantes y profesionales del área pesquera y ecológica en general. El cursillo estuvo dividido en 4 
sesiones

Las sesiones fueron las siguientes:

* Introducción a la Inferencia Bayesiana: Teorema de Bayes, verosimilitud, distribución previa, distribución posterior, 
etc.
* Aspectos computacionales, 1: Integración de Monte Carlo (Gibbs, Metropoli, BUGS), etc.
* Aspectos computacionales, 2: aproximación de Laplace, Integrated Nested Laplace Approximation (INLA).
* Estadística espacial: Modelos condicionales autorregresivos (‘CAR’), inclusión de elementos temporales.
